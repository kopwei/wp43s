;*************************************************************
;*************************************************************
;**                                                         **
;**                    COMPLEX34 ^ ...                      **
;**                    ... ^ COMPLEX34                      **
;**                                                         **
;*************************************************************
;*************************************************************
In: FL_SPCRES=0 FL_CPXRES=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnPower



;==================================================================
; Complex34 ^ Long Integer      see in power_longInteger.txt
; Complex34 ^ Real16            see in power_real16.txt
; Complex34 ^ Complex16         see in power_complex16.txt
; Complex34 ^ Time              see in power_time.txt
; Complex34 ^ Date              see in power_date.txt
; Complex34 ^ String            see in power_string.txt
; Complex34 ^ Real16 Matrix     see in power_real16Matrix.txt
; Complex34 ^ Complex16 Matrix  see in power_complex16Matrix.txt
; Complex34 ^ Short Integer     see in power_shortInteger.txt
; Complex34 ^ Real34            see in power_real34.txt
;==================================================================



;=======================================
; Complex34 ^ Complex34 --> Complex34
;=======================================
In:  FL_ASLIFT=0 FL_CPXRES=0 RY=Cplx:"1.23456 i -6.78" RX=Cplx:"-1.254 i 7.541235"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"-1.254 i 7.541235" RX=Cplx:"-2643.5259601628883080378974068256945580068612144918176355187774 i -1780.31910638893445028977232041694885428536067215736423604308"
