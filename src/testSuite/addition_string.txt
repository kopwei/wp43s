;*************************************************************
;*************************************************************
;**                                                         **
;**                    STRING + ...                         **
;**                    ... + STRING                         **
;**                                                         **
;*************************************************************
;*************************************************************
In: FL_SPCRES=0 FL_CPXRES=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnAdd



;==================================================================
; String + Long Integer  see in addition_longInteger.txt
; String + Real16        see in addition_real16.txt
; String + Complex16     see in addition_complex16.txt
; String + Time          see in addition_time.txt
; String + Date          see in addition_date.txt
;==================================================================



;=======================================
; String + String --> String
;=======================================
In:  FL_ASLIFT=0 FL_CPXRES=0 RY=Stri:"String test " RX=Stri:"WP43S!"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Stri:"WP43S!" RX=Stri:"String test WP43S!"



;=======================================
; String + Real16 Matrix
;=======================================

;=======================================
; Real16 Matrix + String
;=======================================



;=======================================
; String + Complex16 Matrix
;=======================================

;=======================================
; Complex16 Matrix + String
;=======================================



;=======================================
; String + Short Integer --> String
;=======================================
In:  FL_ASLIFT=0 RY=Stri:"String test " RX=ShoI:"1234ABCD#16"
Out: EC=0 FL_ASLIFT=1 RL=ShoI:"1234ABCD#16" RX=Stri:"String test 12\xa0\x0834\xa0\x08AB\xa0\x08CD\xa4\x6f"

;=======================================
; Short Integer + String --> Error24
;=======================================
In:  FL_ASLIFT=0 RY=ShoI:"12540#9" RX=Stri:"String test"
Out: EC=24 FL_ASLIFT=0 RX=Stri:"String test"



;=======================================
; String + Real34 --> String
;=======================================
In:  FL_ASLIFT=0 RY=Stri:"String test " RX=Real:"-12.34"
Out: EC=0 FL_ASLIFT=1 RL=Real:"-12.34" RX=Stri:"String test -12.34"

In:  FL_ASLIFT=0 RY=Stri:"String test " RX=Real:"-12.34":DEG
Out: EC=0 FL_ASLIFT=1 RL=Real:"-12.34":DEG RX=Stri:"String test -12.34\x80\xb0"

In:  FL_ASLIFT=0 RY=Stri:"String test " RX=Real:"-12.345678":DMS
Out: EC=0 FL_ASLIFT=1 RL=Real:"-12.345678":DMS RX=Stri:"String test -12\x80\xb020'44.44\""

In:  FL_ASLIFT=0 RY=Stri:"String test " RX=Real:"-12.345678":GRAD
Out: EC=0 FL_ASLIFT=1 RL=Real:"-12.345678":GRAD RX=Stri:"String test -12.345\xa0\x08678\xa4\xa2"

In:  FL_ASLIFT=0 RY=Stri:"String test " RX=Real:"-12.345678":RAD
Out: EC=0 FL_ASLIFT=1 RL=Real:"-12.345678":RAD RX=Stri:"String test -12.345\xa0\x08678\xa4\xad"

In:  FL_ASLIFT=0 RY=Stri:"String test " RX=Real:"-12.345678":MULTPI
Out: EC=0 FL_ASLIFT=1 RL=Real:"-12.345678":MULTPI RX=Stri:"String test -12.345\xa0\x08678\x83\xc0"

;=======================================
; Real34 + String --> Error24
;=======================================
In:  FL_ASLIFT=0 RY=Real:"12540" RX=Stri:"String test"
Out: EC=24 FL_ASLIFT=0 RX=Stri:"String test"

In:  FL_ASLIFT=0 RY=Real:"12540":GRAD RX=Stri:"String test"
Out: EC=24 FL_ASLIFT=0 RX=Stri:"String test"



;=======================================
; String + Complex34 --> String
;=======================================
In:  FL_ASLIFT=0 FL_CPXRES=0 RY=Stri:"String test " RX=Cplx:"-12.34 i 52"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Cplx:"-12.34 i 52" RX=Stri:"String test -12.34\xa0\x0a+i\x80\xd752."

;=======================================
; Complex34 + String --> Error24
;=======================================
In:  FL_ASLIFT=0 RY=Cplx:"12540 i 5" RX=Stri:"String test"
Out: EC=24 FL_ASLIFT=0 RX=Stri:"String test"
