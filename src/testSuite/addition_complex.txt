;*************************************************************
;*************************************************************
;**                                                         **
;**                    COMPLEX34 + ...                      **
;**                    ... + COMPLEX34                      **
;**                                                         **
;*************************************************************
;*************************************************************
In: FL_SPCRES=0 FL_CPXRES=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnAdd



;==================================================================
; Complex34 + Long Integer      see in addition_longInteger.txt
; Complex34 + Real16            see in addition_real16.txt
; Complex34 + Complex16         see in addition_complex16.txt
; Complex34 + Time              see in addition_time.txt
; Complex34 + Date              see in addition_date.txt
; Complex34 + String            see in addition_string.txt
; Complex34 + Real16 Matrix     see in addition_real16Matrix.txt
; Complex34 + Complex16 Matrix  see in addition_complex16Matrix.txt
; Complex34 + Short Integer     see in addition_shortInteger.txt
; Complex34 + Real34            see in addition_real34.txt
;==================================================================



;=======================================
; Complex34 + Complex34 --> Complex34
;=======================================
In:  FL_ASLIFT=0 FL_CPXRES=0 RY=Cplx:"123.456 i -6.78" RX=Cplx:"-12540 i 7.541235"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"-12540 i 7.541235" RX=Cplx:"-12416.544 i 0.761235"

In:  FL_ASLIFT=0 FL_CPXRES=0 RY=Cplx:"NaN i -6.78" RX=Cplx:"-12540 i 7.541235"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"-12540 i 7.541235" RX=Cplx:"NaN i 0.761235"

In:  FL_ASLIFT=1 FL_CPXRES=0 RY=Cplx:"123.456 i NaN" RX=Cplx:"-12540 i 7.541235"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"-12540 i 7.541235" RX=Cplx:"-12416.544 i NaN"

In:  FL_ASLIFT=0 FL_CPXRES=0 RY=Cplx:"123.456 i -6.78" RX=Cplx:"NaN i 7.541235"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"NaN i 7.541235" RX=Cplx:"NaN i 0.761235"

In:  FL_ASLIFT=1 FL_CPXRES=0 RY=Cplx:"123.456 i -6.78" RX=Cplx:"-12540 i NaN"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"-12540 i NaN" RX=Cplx:"-12416.544 i NaN"
